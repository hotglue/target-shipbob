"""ShipBob target sink class, which handles writing streams."""


from target_shipbob.client import ShipBobSink, ShipBobBatchSink
from typing import List
import json


class ProductSink(ShipBobBatchSink):
    """ShipBob target sink class."""

    name = "Items"
    endpoint = "/product/batch"

    def handle_batch_response(self, response) -> dict:
        results = []
        self.validate_response(response)
        resp_json = response.json()

        if response.status_code == 422:
            self.logger.info(f"Skipping records: {response.request.body}, message: {response.json()}")
            return  {"state_updates": [{"success": True, "existing": True}]}

        if response.status_code != 200:
            self.logger.info(f"Failed to insert Products record: {response.request.body}, message: {response.json()}")
            return  {"state_updates": [{"success": False}]}

        for res in resp_json:
            if res.get("id"):
                results.append({"success": True})
        
        return {"state_updates": results}

    def make_batch_request(self, records: List[dict]):
        final_records = []
        id = None

        for product in records:
            product_name = product.get("name")
            invoice_item = json.loads(product.get("invoice_item", "{}"))
            final_records.append(
                {
                    "reference_id": product.get("sku"),
                    "sku": product.get("sku"),
                    "name": product_name,
                    "unit_price": invoice_item.get("unitPrice"),
                }
            )
        if final_records == []:
            return None

        response = self.request_api("POST", request_data=final_records)
        return response


class SalesOrderSink(ShipBobSink):
    """ShipBob target sink class."""

    name = "SalesOrder"
    endpoint = "/order"

    def search_product(self, produc_name):
        endpoint = "/product"
        params = {"Search": produc_name}
        res = self.request_api("GET", endpoint=endpoint, params=params)
        if res.status_code == 200:
            res = res.json()
            if len(res) > 0:
                return res[0]["id"]
        return False

    def preprocess_record(self, record: dict, context: dict) -> dict:
        shipping_address = record.get("shipping_address", {})
        payload = {
            "shipping_method": record.get("shipping_type", "Free 2-day Shipping"),
            "recipient": {
                "name": record.get("customer_name"),
                "full_name": record.get("customer_name"),
                "address": {
                    "type": shipping_address.get("type", "MarkFor"),
                    "address1": shipping_address.get("line1"),
                    "address2": shipping_address.get("line2"),
                    "city": shipping_address.get("city"),
                    "state": shipping_address.get("state"),
                    "country": shipping_address.get("country"),
                    "zip_code": shipping_address.get("postalCode"),
                },
                "email": record.get("customer_email"),
                "phone_number": record.get("billing_phone"),
            },
            "reference_id": record.get("order_number"),
            "order_number": record.get("order_number"),
            "purchase_date": record.get("transaction_date"),
            "financials": {"total_price": record.get("total_price")},
        }
        # Prepare Products
        products = []
        if record.get("line_items"):
            for line in record.get("line_items"):
                if line.get("id"):
                    products.append(
                        {
                            "id": line["id"],
                            "quantity": line["quantity"],
                        }
                    )
                elif line.get("product_name"):
                    product = self.search_product(line["product_name"])
                    if product:
                        products.append(
                            {
                                "id": product,
                                "quantity": line["quantity"],
                            }
                        )

        if len(products) == 0:
            self.logger.warning(f"No products found in order, record: {record}")
        
        payload.update({"products": products})
        return payload

    def upsert_record(self, record: dict, context: dict):
        state_updates = {
            "success": True,
        }
        response = self.request_api("POST", request_data=record)
        if response.status_code == 422:
            self.logger.info("Skipping record {} with message: ({}) {}".format(
                record,
                response.status_code,
                response.json()
            ))
            state_updates["existing"] = True
            return None, response.ok, state_updates
        
        if response.status_code != 200:
            self.logger.info("Skipping record {} with message: ({}) {}".format(
                record,
                response.status_code,
                response.json()
            ))
            state_updates["success"] = False
            return None, response.ok, state_updates
        
        state_updates["success"] = True
        id = response.json().get("id")
        return id, response.ok, state_updates
