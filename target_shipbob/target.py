"""ShipBob target class."""


from singer_sdk import typing as th
from target_hotglue.target import TargetHotglue

from target_shipbob.sinks import (
    ProductSink,
    SalesOrderSink,
)


class TargetShipBob(TargetHotglue):
    """Sample target for ShipBob."""

    name = "target-shipbob"
    SINK_TYPES = [ProductSink, SalesOrderSink]
    config_jsonschema = th.PropertiesList(
        th.Property("access_token", th.StringType, required=True),
    ).to_dict()


if __name__ == "__main__":
    TargetShipBob.cli()
