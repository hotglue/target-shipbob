import requests

from memoization import cached

from target_hotglue.client import HotglueSink, HotglueBatchSink
from singer_sdk.exceptions import RetriableAPIError, FatalAPIError


class Shipbob:
    channel_id = None
    error_counter = 0

    @property
    def base_url(self) -> str:
        if self.config.get("sandbox"):
            return "https://sandbox-api.shipbob.com/2.0"
        return f"https://api.shipbob.com/1.0"

    @property
    def api_token(self) -> str:
        return self.config.get("access_token")
    
    def validate_response(self, response):
        """Validate HTTP response."""
        if response.status_code == 401:
            raise FatalAPIError(
                f"Unauthorized: {response.status_code} {response.reason} at {self.path}"
            )
        if response.status_code >= 400 and self.config.get("ignore_server_errors", True):
            self.error_counter += 1
        elif 500 <= response.status_code < 600 or response.status_code in [429, 403]:
            msg = (
                f"{response.status_code} Server Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise RetriableAPIError(msg)
        elif 400 <= response.status_code < 500:
            msg = (
                f"{response.status_code} Client Error: "
                f"{response.reason} for path: {self.path}"
            )
            raise FatalAPIError(msg)
        try:
            response.json()
        except:
            raise RetriableAPIError(f"Invalid JSON: {response.text}")

    @property
    @cached
    def get_channel_id(self):
        if self.channel_id:
            return self.channel_id
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "Authorization": f"Bearer {self.api_token}",
        }
        url = f"{self.base_url}/channel"
        resp = requests.get(url=url, headers=headers)

        if resp.status_code != 200:
            raise FatalAPIError("Failed to request default channel: {}".format(resp.text))
        
        resp = resp.json()
        return str(resp[0].get("id"))

    @property
    @cached
    def http_headers(self):
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "Authorization": f"bearer {self.api_token}",
            "shipbob_channel_id": self.get_channel_id,
        }
        return headers


class ShipBobSink(Shipbob, HotglueSink):
    pass


class ShipBobBatchSink(Shipbob, HotglueBatchSink):
    pass
